//Project related.
var path = require("path");
var site = "wp-starter.dev";
var theme = path.basename(__dirname);
var dest = "../../../www/wp-content/themes/" + theme + "/";

//gulp general libraries
var gulp = require("gulp");
var notify = require("gulp-notify");
var rename = require("gulp-rename");
var gutil = require("gulp-util");

//gulp CSS libraries
var cssGlobbing = require("gulp-css-globbing");
var postcss = require("gulp-postcss");
var precss = require("precss");
var cssnext = require("cssnext");
var lost = require("lost");
var csso = require("gulp-csso");

//gulp JS Librariers
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var browserify = require("browserify");
var babel = require("babelify");

//gulp IMG libraries
var imagemin = require("gulp-imagemin");
var svgSprite = require("gulp-svg-sprite");

//other libraries
var browserSync = require("browser-sync");
var reload = browserSync.reload;
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var del = require("del");

gulp.task("clean", function() {
  return del.sync([dest], {force: true});
});

gulp.task("styles", function() {
  var processors = [
    precss(),
    lost(),
    cssnext({compress:true})
  ];
  gulp.src("src/css/main.css")
    .pipe(cssGlobbing())
    .pipe(postcss(processors))
    .pipe(rename("style.css"))
    .pipe(gutil.env.prod ? csso() : gutil.noop())
    .pipe(gulp.dest(dest))
    .pipe(reload({stream: true}));
});

gulp.task("scripts", function() {

  var b = browserify({
    entries: "./src/js/App.js",
    debug: true
  }).transform(babel);

  b.bundle()
    .on("error", function(e){ gutil.log("browserify: " + e); })
    .pipe(source("App.js"))
    .pipe(gutil.env.prod ? buffer() : gutil.noop())
    .pipe(gutil.env.prod ? uglify() : gutil.noop())
    .pipe(rename("main.js"))
    .pipe(gulp.dest(dest + "/js"))
    .pipe(reload({stream: true}));
});

gulp.task("images", function() {
  gulp.src("src/img/*")
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: true}],
    }))
    .pipe(gulp.dest(dest + "/img"))
    .pipe(reload({stream: true}));
});

gulp.task("icons", function() {
  var config = {
    shape: {
      id: {
        generator: "icon--%s"
      }
    },
    mode: { symbol : true }
  };
  gulp.src("src/icons/*.svg")
    .pipe(svgSprite(config))
    .pipe(gulp.dest(dest + "/icons"))
    .pipe(reload({stream: true}));
});

gulp.task("fonts", function() {
  gulp.src("src/fonts/*")
    .pipe(gulp.dest(dest + "/fonts"));
});

gulp.task("source", function() {
  gulp.src("src/**/*.php")
    .pipe(gulp.dest(dest))
    .pipe(reload({stream: true}));

  gulp.src("src/**/*.twig")
    .pipe(gulp.dest(dest))
    .pipe(reload({stream: true}));
});

gulp.task("serve", function() {
  browserSync({proxy: site});
  gulp.watch("src/css/**/*.css", ["styles"]);
  gulp.watch("src/js/**/*.js", ["scripts"]);
  gulp.watch("src/img/**/*", ["images"]);
  gulp.watch("src/icons/**/*.svg", ["icons"]);
  gulp.watch("src/**/*.php", ["source"]);
  gulp.watch("src/**/*.twig", ["source"]);
});

gulp.task("default", ["clean", "source", "fonts", "styles", "scripts", "images", "icons"], function(){

  if (gutil.env.prod)
  {
    gutil.noop();
  }
  else
  {
    gulp.start("serve");
  }

});
