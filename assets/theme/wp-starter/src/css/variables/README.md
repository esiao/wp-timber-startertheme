#Variables
This directory should only contain css files to express the different variables used in the theme.
For instance :
fonts.css -> for the fonts definitions & variables
colors.css -> for the colors variables
sizes.css -> for the sizes variables

By convention we'll use filename--variable for the variable name.
