<?php

/**
 * Load more TYPE on AJAX call
 * @param array $exclude_ids
 * @param int $pad posts_per_page
 * @param string $filter if there's a filter
 */
function NAMESPACE_TYPE_load_more() {

	$exclude_ids = $_POST['exclude_ids'];
	$pad = $_POST['pad'];
	if (isset($_POST['filter']) && is_array($_POST['filter'])) {
		$filter = implode(",", $_POST['filter']);
	}

	$post_type = 'post';
	$taxonomy_name = 'category_name';

	if (isset($filter) && $filter !== '*') {

		//Array for the filtered query
		$args = array(
			'post_type'=> $post_type,
			'posts_per_page' => $pad,
			'post__not_in' => $exclude_ids,
			$taxonomy_name => $filter
		);

	} else {

		//Array for general query
		$args = array(
			'post_type'=> $post_type,
			'post__not_in' => $exclude_ids,
			'posts_per_page' => $pad
		);

	}

	//Retrieve the posts
	$ajax_posts = Timber::get_posts($args);

	//Render them in data
	foreach ($ajax_posts as $key => $post) {

		$context['post'] = $post;

		//Load the template and compile it
		$post_template = Timber::compile( 'teaser-'. $post_type .'.twig', $context );

		//Remove all the newlines to send clean JSON
		$data[$key] = trim(preg_replace('/\s+/', ' ', $post_template));

	}

	if (isset($data)) {

		//Encode the data (posts array) and send it
		die(json_encode($data));

	} else {

		//Nothing found
		die(false);

	}

}
add_action( 'wp_ajax_NAMESPACE_TYPE_load_more', 'NAMESPACE_TYPE_load_more' );
add_action( 'wp_ajax_nopriv_NAMESPACE_TYPE_load_more', 'NAMESPACE_TYPE_load_more' );
