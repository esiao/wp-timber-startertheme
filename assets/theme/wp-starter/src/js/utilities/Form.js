const $ = require("zepto-browserify").$
import _ from "lodash";

class FormStyleInputFile {
  constructor(input, placeholder, action) {

    //Create the input structure
    this._$inputFile = $(input),
    this._$inputFileParent = this._$inputFile.wrap("<div class='InputFile'></div>").parent(),
    this._$inputFilePlaceholder = $("<span class='InputFile-placeholder'>" + placeholder + "</span>");
    this._$inputFileAction = $("<span class='InputFile-action'>" + action + "</span>");

    //Inject it in the DOM
    this._$inputFilePlaceholder.prependTo(this._$inputFileParent);
    this._$inputFileAction.prependTo(this._$inputFileParent);

    //Listen to the click event on the placeholder and delegate it to the actual hidden input
    this._$inputFilePlaceholder.add(this._$inputFileAction).on("click", $.proxy(f => {
      this._$inputFile.trigger("click");
    }), this);

    //Listen to the change event on the hidden input to change the placeholder value
    $inputFile.on("change", $.proxy(this._updatePlaceholderValue, this));
  }
  _updatePlaceholderValue() {
    //Regex to get file name
    let filePath = $inputFile.val();
    let fileName = filePath.match(/[^\\]+\.[a-z]+$/ig)[0];

    //If the file name is more than 30 characters long trim it
    if (fileName.length > 30) {
      fileName = fileName.match(/^.{1,30}/)[0] + "...";
    }

    //Replace the placeholder value with the file name
    this._$inputFilePlaceholder.text(fileName);
  }
}

class FormWrapErrors {
  constructor(inputWrapper, errorClass, formSubmit, method) {

    this._$inputWrapper = $(inputWrapper);
    this._$formSubmit = $(formSubmit);
    this._errorClass = errorClass;

    if (method == "ajax")
    {
      //If the form use Ajax listen to ajax events
      $(document).on("ajaxComplete", $.proxy(this._toggleErrors, this));
    }
    else if (method == "wpcf7")
    {
      //If contact form 7 use built-in event
      jQuery(document).on("wpcf7:invalid", ".wpcf7", $.proxy(this._toggleErrors, this));
    }
    else
    {
      //Listen to the click event on the form
      this._$formSubmit.on("click", $.proxy(f => {
        setTimeout($.proxy(this._toggleErrors, this), 100);
      }), this);
    }

  }
  _toggleErrors() {

    this._$inputWrapper.each((index, wrapper) => {
      let $wrapper = $(wrapper);
      let error = $wrapper.find(this._errorClass).length ? true : false;

      //If the input has the errorClass then add a class to the wrapper to style it
      $wrapper.toggleClass('is-formError', error);
    });

  }
}

export{FormStyleInputFile, FormWrapErrors};
