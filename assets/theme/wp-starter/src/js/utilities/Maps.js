import $ from 'jquery';
import _ from 'lodash';

class Maps {
  constructor(map, zoom = 16, options, centerOnFirstMarker = false) {
    //Get map elements
    this._$map = $(map);
    //Grap all map markers
    this._$markers = this._$map.find('.Map-marker');
    //Map zoom
    this._zoom = zoom;
    //Is the map centered only on the first marker
    this._centerOnFirstMarker = centerOnFirstMarker;

    //Create a new road type map
    this._map = new google.maps.Map(this._$map[0], {
      zoom: this._zoom,
      center: new google.maps.LatLng(0, 0),
      mapTypeId	: google.maps.MapTypeId.ROADMAP
    });

    //If styles are defined
    if (WPO.map.styles && WPO.map.styles.length > 0) {
      this._map.setOptions({styles: JSON.parse(WPO.map.styles)});
    }

    //If cutsom options were defined
    if (options) {
      this._map.setOptions(options);
    }

    //Create a variable to store the markers
    this._markers = [];

    //For each marker on the map render them
    var that = this;
    this._$markers.each(function(){
      that._addMarker($(this));
    });

    //Center the map
    this._center();

    $(window).on('resize',_.bind(function(){
      google.maps.event.trigger(this._map, 'resize');
      this._center();
    }, this));
  }
  _addMarker($marker) {
    //Get the marker position
    var pos = new google.maps.LatLng(parseFloat($marker.data('lat')), parseFloat($marker.data('lng')));
    var cat = $marker.data('category');

    //Create a marker
    var marker;

    //If the marker has a category attached
    if (cat && cat.length)
    {
      var icon = $marker.data('icon');
      var image = {
        url: WPO.map.category.url[icon],
        size: new google.maps.Size(WPO.map.category.size[0], WPO.map.category.size[1]),
        origin: new google.maps.Point(WPO.map.category.origin[0], WPO.map.category.origin[1]),
        anchor: new google.maps.Point(WPO.map.category.anchor[0], WPO.map.category.anchor[1]),
        scaledSize: new google.maps.Size(WPO.map.category.scaled[0], WPO.map.category.scaled[1])
      };
      marker = new google.maps.Marker({
        position: pos,
        map: this._map,
        icon: image,
        category: cat
      });
    }
    //If the WPO object contains a custom marker
    else if (WPO.map.marker)
    {
      var image = {
        url: WPO.map.marker.url,
        size: new google.maps.Size(WPO.map.marker.size[0], WPO.map.marker.size[1]),
        origin: new google.maps.Point(WPO.map.marker.origin[0], WPO.map.marker.origin[1]),
        anchor: new google.maps.Point(WPO.map.marker.anchor[0], WPO.map.marker.anchor[1]),
        scaledSize: new google.maps.Size(WPO.map.marker.scaled[0], WPO.map.marker.scaled[1])
      };
      marker = new google.maps.Marker({
        position: pos,
        map: this._map,
        icon: image
      });
    }
    //Classic Gmap marker
    else
    {
      marker = new google.maps.Marker({
        position: pos,
        map: this._map
      });
    }

    //Store the marker on the markers object
    this._markers.push(marker);
  }
  _center() {

    //Get map bounds
    var bounds = new google.maps.LatLngBounds();

    var that = this;
    //For each marker extends bounds with its position
    $.each(this._markers, function( i, marker ){

      //Only the first marker coordinates
      if (that._centerOnFirstMarker)
      {
        //If it's the first marker add the coords
        if (i === 0) {
          var pos = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
          bounds.extend( pos );
        }
      }
      //Get all marker coordinates and extend bounds
      else
      {
        var pos = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
        bounds.extend( pos );
      }
    });

    //If there's only one marker center the map on the marker
    if( this._markers.length == 1 || this._centerOnFirstMarker)
    {
      this._map.setCenter( bounds.getCenter() );
      this._map.setZoom( this._zoom );
    }
    else
    {
      this._map.fitBounds( bounds );
    }

  }
  _offset(offsetX,offsetY) {

    //Retrieve map center
    var center = this._map.getProjection().fromLatLngToPoint(
      (this._map.latlng instanceof google.maps.LatLng) ? this._map.latlng : this._map.getCenter()
    );

    //Get the passed offset and created a point on the map
    var offset = new google.maps.Point(
      ( (typeof(offsetX) == 'number' ? offsetX : 0) / Math.pow(2, this._map.getZoom()) ) || 0,
      ( (typeof(offsetY) == 'number' ? offsetY : 0) / Math.pow(2, this._map.getZoom()) ) || 0
    );

    //Use the difference between the center and the offset point to recenter the map
    this._map.setCenter(this._map.getProjection().fromPointToLatLng(new google.maps.Point(
      center.x - offset.x,
      center.y + offset.y
    )));

  }
  _filter(categories) {

    var map = this._map;

    for (var i = 0; i < this._markers.length; i++) {

      var marker = this._markers[i];

      if (typeof categories === 'object')
      {

        var markerMap = null;

        if (_.size(categories))
        {
          $.each(categories, function( index, category ) {

            if (!marker.category || marker.category == category)
            {
              markerMap = map;
            }

          });
        }
        else if (!marker.category) {
          markerMap = map;
        }

      }
      else if (typeof categories === 'string')
      {
        var category = categories;
        var markerMap = !marker.category || marker.category == category ? map : null;
      }

      this._markers[i].setMap(markerMap);

    }

  }
}
export {Maps};
