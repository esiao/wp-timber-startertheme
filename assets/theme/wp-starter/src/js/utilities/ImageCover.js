const $ = require("zepto-browserify").$;
import _ from "lodash";

class ImageCover {
  constructor(container) {
    this._container = container;
    this._$container = $(container);
    this._resizeAndPositionImage();

    $(window).on("resize", $.proxy(_.throttle(this._resizeAndPositionImage, 250), this));
  }
  _refresh() {
    this._$container = $(this._container);
    this._resizeAndPositionImage();
  }
  _resizeAndPositionImage() {
    this._$container.each(function() {

      //Get Image and Container information
      var containerH = $(this).height() > 0 ? $(this).height() : parseInt($(this).css("padding-bottom"), 10),
          containerW = $(this).width(),
          $image = $(this).find("img");

      //When the image is loaded process it to center it inside the container.
      $image.on("load", f => {

        //Reset image styles
        $image.css({
          "max-width": "none",
          "width": "auto",
          "height": "auto",
          "position": "absolute",
          "transform": "none",
        });

        //Get image dimensions
        var imageH = $image.height(),
            imageW = $image.width();

        //Portrait
        if (imageH > imageW)
        {
          //Set the width to fit container
          $image.css("width", "100%");

          //Check that the new height is taller than the container height
          //If not invert
          if($image.height() < containerH) {
            $image.css({
              "width": "auto",
              "height": "100%"
            });
          }
        }
        //Landscape
        else
        {
          //Set the image height to fit container
          $image.css("height", "100%");

          //Check that the new width is taller than the container width
          //If not invert
          if($image.width() < containerW) {
            $image.css({
              "width": "100%",
              "height": "auto"
            });
          }
        }

        //Calculate the new image sizes
        var newH = $image.height(),
            newW = $image.width();

        //Offset the image to center it inside the container
        $image.css({
          left: "-"+ (newW - containerW) / 2 +"px",
          top: "-"+ (newH - containerH) / 2 +"px"
        });

      });

      //If images are served from cache trigger load.
      if ($image[0].complete) {
        $image.trigger("load");
      }

    });
  }
}

export {ImageCover};
