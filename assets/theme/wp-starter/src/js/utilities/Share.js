const $ = require("zepto-browserify").$
import _ from "lodash";

class Share {
  constructor(link) {
    this._socialTab = {
      twitter: "https://twitter.com/intent/tweet?text={title}&url={url}",
      facebook: "https://www.facebook.com/sharer/sharer.php?u={url}",
      googleplus: "https://plus.google.com/share?url={url}",
      pinterest: "http://pinterest.com/pin/create/button/?url={url}&media={media}&description={title}"
    }
    this._popup = {
      height: 516,
      width: 838
    }
    this._link = link;

    this._init();
  }
  _init() {
    $(this._link).each(function(){
      let $link = $(this);
      let count = $link.data("count");
      let type = $link.data("type");
      let url = $link.data("url");
      let request;

      if (count !== undefined && url !== undefined) {
        if (type === "twitter") {
          request = $.ajax({
            url: "http://cdn.api.twitter.com/1/urls/count.json?url=" + url + "&callback=?",
            method: "GET",
            dataType: "json",
            contentType: "text/plain",
            xhrFields: {
              withCredentials: false
            }
          });
          request.done(function(data){
            if (data.count) {
              $link.attr("data-count", data.count);
            }
          });
        } else if (type === "facebook") {
          request = $.ajax({
            url: "http://graph.facebook.com/?id=" + url,
            method: "GET",
            dataType: "json",
            contentType: "text/plain",
            xhrFields: {
              withCredentials: false
            }
          });
          request.done(function(data){
            if (data.shares) {
              $link.attr("data-count", data.shares);
            }
          });
        }
      }
    }).on("click", _.bind(this._share, this));
  }
  _share(event) {

    event.preventDefault();

    let $link = $(event.currentTarget);
    let url = $link.data("url");
    let title = $link.data("title");
    let type = $link.data("type");
    let media = $link.data("media");
    let top = ($(window).height() - this._popup.height)/2;
    let left = ($(window).width() - this._popup.width)/2;

    if (url !== ""){
      this._socialTab[type] = this._socialTab[type].replace("{url}", encodeURIComponent(url));
    } else {
      this._socialTab[type] = this._socialTab[type].replace("{url}", "");
    }

    if (title !== ""){
      this._socialTab[type] = this._socialTab[type].replace("{title}", encodeURIComponent(title));
    } else {
      this._socialTab[type] = this._socialTab[type].replace("{title}", "");
    }

    if (media !== "") {
      this._socialTab[type] = this._socialTab[type].replace("{media}", media);
    } else {
      this._socialTab[type] = this._socialTab[type].replace("{media}", "");
    }

    let windowInfo = `height=${this._popup.height}, width=${this._popup.width}, top=${top}, left=${left}`;
    var newWindow = window.open(this._socialTab[type], "", windowInfo);
    if (window.focus) newWindow.focus();
    return false;

  }
}

export {Share};
