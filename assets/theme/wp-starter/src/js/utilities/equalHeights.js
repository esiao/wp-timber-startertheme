const $ = require("zepto-browserify").$;
import _ from "lodash";

class EqualHeights {
  constructor(items) {
    this._$items = $(items);
    this._resizeElements();

    $(window).on("resize", $.proxy(_.throttle(this._resizeElements, 250), this));
  }
  _resizeElements() {
    //Set height based on first element
    let H = this._$items.css('height','auto').height();

    //Go through each element and compare it's height to the storred one
    this._$items.each(function(){
      let elHeight = $(this).height();
      H = elHeight > H ? elHeight : H;
    });

    //Set the heighest height for all items
    this._$items.css('height', H);
  }
}
export {EqualHeights};
