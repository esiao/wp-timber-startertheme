const $ = require("zepto-browserify").$;
import _ from "lodash";

class AjaxLoadMore {
  constructor(btn, container, post, delay = 300, callback) {

    this._settings = {};
    this._delay = delay;
    this._callback = callback;
    this._url = WPO.ajax.url;

    this._$container = $(container);
    this._$btn = $(btn);
    this._$loader = $(WPO.ajax.loader);
    this._post = post;

    this._total = this._$container.attr("data-total");
    let type = this._$container.attr("data-type");
    let pad = this._$container.attr("data-pad");
    let excludeIds = [];

    let $posts = $(post);
    _.forEach($posts, (post, i) => {
      let $post = $(post);
      let id = $post.attr("data-post-id");

      excludeIds.push(id);
    });

    if (type.length)
    {
      //Settings for current post-type ajax container
      this._settings = {
        action: WPO.ajax.action[type],
        pad: pad,
        total: this._total,
        filter: "*",
        excludeIds: excludeIds,
        loadMoreClass: "is-loadingMore",
        completeClass: "is-complete",
      }
    }

    //When clicking on the load more button load more entries
    this._$btn.on("click", $.proxy((e) => {
      e.preventDefault();
      this._loadMore();
    }, this));
  }
  _infiniteScroll(item) {

    let $items = $(item);
    let $lastItem = $items.last();
    let isTotal = $items.length >= this._settings.total;
    let ajaxRuning = this._$container.hasClass(this._settings.loadMoreClass);

    //Check if we reached last item on list offset top. And proceed a load more.
    if ($lastItem)
    {
      let limitReached = $(window).scrollTop() + $(window).height() >= $lastItem.offset().top;
      if (limitReached && !isTotal)
      {
        this._loadMore();
      }
    }

  }
  _initFilters(filter, callback) {

    this._filter = filter;
    this._filterCallback = callback;
    this._settings.filters = "*";
    this._filterTotal = 0;

    $(this._filter).on("click", $.proxy(this._filterPosts, this));
  }
  _filterPosts() {

    let $filter = $(event.currentTarget);
    let filter = $filter.attr("data-filter");
    let isFilterActive = _.indexOf(this._settings.filters, filter) >= 0;
    let filterClass = "*";
    let total = 0;

    $filter.toggleClass("is-active", !isFilterActive);

    //Check if the filter is active and push it's value inside the filters
    if (isFilterActive)
    {
      this._settings.filters = _.without(this._settings.filters, filter);
      if (this._settings.filters.length === 0) {
        this._settings.filters.push("*");
      }
    }
    //No filter active use *
    else
    {
      this._settings.filters = _.without(this._settings.filters, "*");
      this._settings.filters.push(filter);
    }

    //Not the * filter.
    if (_.indexOf(this._settings.filters, "*") === -1)
    {
      //Join filters to create a class.
      filterClass = "." + this._settings.filters.join(", .");
    }

    //Check if the maximum post for current filter selection is reached.
    this._checkCurrentPostsLoaded();

    //If there's a callback function for our filters call it.
    if (typeof this._filterCallback == "function")
    {
      this._filterCallback.call(this, this._settings.filters);
    }
  }
  _loadMore() {
    //Check if there's not already a request going on.
    if (this._$container.hasClass(this._settings.loadMoreClass)) {
      return false;
    }

    //Add a class to the container to prevent loading more items while loading.
    this._$container.addClass(this._settings.loadMoreClass);

    //Request posts.
    $.post(
      this._url,
      {
        "action": this._settings.action,
        "pad": this._settings.pad,
        "filter": this._settings.filters,
        "exclude_ids": this._settings.excludeIds,
      },
      $.proxy(this._appendNewContent, this)
    )
  }
  _appendNewContent(data) {
    if (data)
    {
      //Get the elements to append.
      let posts = JSON.parse(data);
      let $posts = [];
      $.each(posts, $.proxy((index, post) => {

        let $post = $(post);
        let id = $post.attr("data-post-id");

        this._settings.excludeIds.push(id);

        //Populate posts array for callback.
        $posts.push($post[0]);
        $post.addClass("is-hidden").appendTo(this._$container);

        setTimeout(f => {

          //Show element..
          $post.removeClass("is-hidden");

          //Remove the container class preventing to load more elements.
          if (index + 1 == posts.length)
          {
            this._$container.removeClass(this._settings.loadMoreClass);
          }

        }, index * this._delay);

      }, this));

      //Remove the loader.
      this._$loader.remove();

      //Check if the maximum post for current filter selection is reached.
      this._checkCurrentPostsLoaded();

      //If there's a callback defined and it's a function call it.
      if (typeof this._callback == "function")
      {
        this._callback.call(this, $posts);
      }

    }
    else
    {
      this._$container.removeClass(this._settings.loadMoreClass);
      this._$btn.addClass("is-hidden");
    }
  }
  _checkCurrentPostsLoaded() {

    let hideBtn = false;
    let totalPosts = $(this._post).length;

    //Not the * filter.
    if (_.indexOf(this._settings.filters, "*") === -1)
    {

      let allPostsLoadedForFilter = [];

      //Loop on filters and for each count the number of posts to see if there's more to load
      _.forEach(this._settings.filters, (filter, i) => {
        let $filter = $(this._filter + "[data-filter="+ filter +"]");
        let filterTotal = $filter.attr("data-total");

        let $posts = $(this._post + "." + filter);
        let totalPosts = $posts.length;

        allPostsLoadedForFilter[i] = totalPosts == filterTotal;
      });

      hideBtn = _.indexOf(allPostsLoadedForFilter, false) === -1;

    }
    else if (this._total == totalPosts)
    {
      hideBtn = true;
    }

    this._$btn.toggleClass("is-hidden", hideBtn);
  }
}

export{AjaxLoadMore};
