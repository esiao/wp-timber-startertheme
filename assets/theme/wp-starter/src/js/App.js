'use_strict';

import $ from 'jquery';
import _ from 'lodash';

import {Page} from './pages/Page';

class App {
  constructor() {
     this._setIsMobile();

     $(window).on('scroll', _.bind(_.throttle(this._onScroll, 100), this));
     $(window).on('resize', _.bind(_.throttle(this._onResize, 250), this));
     $(window).on('resize', _.bind(_.debounce(this._onResizeEnd, 250), this));

     if ($('.Page').length) {
       this._page = new Page(this);
     }

  }
  _setIsMobile() {
    this._isMobile = window.matchMedia('(max-width: 61.9em)').matches;
  }
  _onScroll() {
    if (this._page && this._page.scroll) {
      this._page.scroll(this);
    }
  }
  _onResize() {
    this._setIsMobile();
    if (this._page && this._page.resize) {
      this._page.resize(this);
    }
  }
  _onResizeEnd() {

  }
}

$(document).ready(function () {
  var app = new App();
});
